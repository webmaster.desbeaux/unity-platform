﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
    public void LoadLevelPassed(string level)
    {
        SceneManager.LoadScene(level);
    }
}
