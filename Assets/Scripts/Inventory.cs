﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    public int coinsCount = 0;
    public Text coinsCountText;

    //Items
    public List<Item> content = new List<Item>();
    private int contentCurrentIndex = 0;
    public Image itemUIImage;
    public Sprite emptyItemImage;

    public PlayerEffect playerEffect;

    [HideInInspector]
    public static Inventory instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance d'inventaire dans la scène");
            return;
        }

        instance = this;
    }

    private void Start()
    {
        UpdateInventoryUI();
    }

    public void AddCoins(int count)
    {
        coinsCount += count;
        UpdateTextUI();
    }

    public void RemoveCoins(int count)
    {
        coinsCount -= count;
        UpdateTextUI();
    }

    public void UpdateTextUI()
    {
        coinsCountText.text = coinsCount.ToString();
    }

    public void ConsumeItem()
    {
        if (content.Count == 0)
            return;

        Item currentItem = content[contentCurrentIndex];
        PlayerHealth.instance.HealPlayer(currentItem.hpGiven);
        playerEffect.AddSpeed(currentItem.speedGiven, currentItem.speedDuration);

        content.Remove(currentItem);

        GetNextIndex();
        UpdateInventoryUI();
    }

    public void GetNextIndex()
    {
        if (content.Count == 0)
            return;

        contentCurrentIndex++;
        if (contentCurrentIndex > content.Count - 1)
            contentCurrentIndex = 0;
        UpdateInventoryUI();
    }

    public void GetPreviousIndex()
    {
        if (content.Count == 0)
            return;

        contentCurrentIndex--;
        if (contentCurrentIndex < 0)
            contentCurrentIndex = content.Count - 1;
        UpdateInventoryUI();
    }

    public void UpdateInventoryUI()
    {
        if (content.Count > 0)
            itemUIImage.sprite = content[contentCurrentIndex].image;
        else
            itemUIImage.sprite = emptyItemImage;
    }
}
