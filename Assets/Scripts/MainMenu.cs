﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string levelToLoad;

    public GameObject settingWindow;

    public void StartGame()
    {
        SceneManager.LoadScene(levelToLoad);
    }
    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void SettingsButton()
    {
        settingWindow.SetActive(true);
    }

    public void CloseSettingsWindows()
    {
        settingWindow.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
