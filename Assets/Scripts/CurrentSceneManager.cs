﻿using UnityEngine;

public class CurrentSceneManager : MonoBehaviour
{
    public int coinsPickUpInThisSceneCount;
    public Vector3 respawnPoint;

    public static CurrentSceneManager instance;

    public void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instance de CurrentSceneManager dans la scène");
            return;
        }

        instance = this;

        respawnPoint = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

}
